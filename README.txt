Lipei Li, LL2857, N11207693
Homework3 -- GIT,JAVACL,MAVEN

Please use the standard set up for a maven project in Intellij to run this homework.

Run SimulationManager.java to get final results, the same as in monte carlo homework.

Might take up to 8 minutes to run. For faster result, change "double error" in SimulationManager.java
to 0.1

I added a new class UniformToGaussianTransformer.java,  which uses GPU to produce gaussians
I added a test for UniformToGaussianTransformer.java


Result for one of the simulations is :

Price of European Call option is:
6.1897 USD with 7090418 iterations. 
Simulation time: 301.821 seconds 

Price of Asian Call option is:
2.1805 USD with 1349577 iterations. 
Simulation time: 79.177 seconds 

Final result should look similar to above.