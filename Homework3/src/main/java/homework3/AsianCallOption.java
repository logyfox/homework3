package homework3;

/**
 * Created by Lipei Li on 2014/12/12.
 */
import java.util.List;

import org.joda.time.DateTime;

public class AsianCallOption implements PayOut{
    private double K;

    public AsianCallOption(double K){
        this.K = K;
    }


    public double getPayout(PathGenerator path) {
        List<Pair<DateTime, Double>> prices = path.getPath();
        double sum = 0;
        for (int i=0; i<prices.size();i++){
            sum = sum + prices.get(i).getValue();
        }
        double average = sum/prices.size();
        return Math.max(0, average - K);
    }
}


