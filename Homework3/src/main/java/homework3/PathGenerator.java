package homework3;

/**
 * Created by Lipei Li on 2014/12/12.
 */
import java.util.List;

import homework3.Pair;

import org.joda.time.DateTime;

public interface PathGenerator {

    public List<Pair<DateTime, Double>> getPath();

}
