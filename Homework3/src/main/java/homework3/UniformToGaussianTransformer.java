package homework3;

/**
 * The code for this class is adjusted from the example6.java of class6
 * We change the kernel such that box muller transformation is implemented
 * to produce standard Gaussian variables;
 *
 * Created by Lipei Li on 2014/12/12.
 */
import com.nativelibs4java.opencl.*;
import org.bridj.Pointer;
import static org.bridj.Pointer.allocateFloats;

public class UniformToGaussianTransformer {
    private int batchSize;

    public UniformToGaussianTransformer(int N){
        batchSize = N;
    }

    public Pointer<Float> getGaussianPointer(){
        // Creating the platform which is out computer.
        CLPlatform clPlatform = JavaCL.listPlatforms()[0];
        // Getting the GPU device
        CLDevice device = clPlatform.getBestDevice();
        //CLDevice device = clPlatform.listGPUDevices(true)[0];
        // Let's make a context
        CLContext context = JavaCL.createContext(null, device);
        // Lets make a default FIFO queue.
        CLQueue queue = context.createDefaultQueue();

        // Read the program sources and compile them :
        // Our kernel implements Box-Muller Transformation to get Gaussian(0,1)
        String src = "__kernel void uniform_gaussian(__global const float* a, __global const float* b, __global float* out, int n) \n" +
                "{\n" +
                "    int i = get_global_id(0);\n" +
                "    if (i >= n)\n" +
                "        return;\n" +
                "\n" +
                "    out[2*i] = sqrt(-2*log(a[i]))*cos(2*3.141592653589793238462643*b[i]);\n" +
                "    out[2*i+1] = sqrt(-2*log(a[i]))*sin(2*3.141592653589793238462643*b[i]);\n"+
                "}";
        CLProgram program = context.createProgram(src);
        program.build();

        CLKernel kernel = program.createKernel("uniform_gaussian");

        final int n = batchSize/2;
        // create 2 Pointer<Float> each with n elements;
        final Pointer<Float>
                aPtr = allocateFloats(n),
                bPtr = allocateFloats(n);

        //let each element be Uniform(0,1)
        for (int i = 0; i < n; i++) {
            aPtr.set(i, (float) Math.random());
            bPtr.set(i, (float) Math.random());
        }


        // Create OpenCL input buffers (using the native memory pointers aPtr and bPtr) :
        CLBuffer<Float>
                a = context.createFloatBuffer(CLMem.Usage.Input, aPtr),
                b = context.createFloatBuffer(CLMem.Usage.Input, bPtr);

        // Create an OpenCL output buffer :
        CLBuffer<Float> out = context.createFloatBuffer(CLMem.Usage.Output, 2*n);
        kernel.setArgs(a,b,out,n);

        CLEvent event = kernel.enqueueNDRange(queue, new int[]{n}, new int[]{100});

        final Pointer<Float> cPtr = out.read(queue,event);

        // return out Pointer cPtr of length 2*n = batchSize
        return cPtr;
    }

}
