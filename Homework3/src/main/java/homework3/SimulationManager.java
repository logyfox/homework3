package homework3;

/**
 * Created by Lipei Li on 2014/12/12.
 */
import org.joda.time.DateTime;

public class SimulationManager {
    public static void main(String[] args){
        // Q1: European Option

        long startTime = System.currentTimeMillis();

        //set arguments of GBMRandomPathGenerator as required
        double S0 = 152.35;
        double sigma = 0.01;
        double rate = 0.0001;
        int N = 252;
        double K = 165;

        //set error and y (as defined in lecture)
        double error = 0.01;
        double y = 2.053749; //calculated from R, with P(|Y|<y) = 0.96

        //only N affects the result; endDate and startDate don't.
        DateTime startDate = new DateTime(2014,1,1,0,0);
        DateTime endDate = new DateTime(2014,2,1,0,0);

        //Get the parts ready
        StatsCollector sc = new StatsCollector();
        GaussianRandomNumberGenerator grng = new GaussianRandomNumberGenerator(N);
        RandomVectorGenerator rvg = new AntiTheticVectorGenerator(grng);
        EuropeanCallOption ec = new EuropeanCallOption(K);
        GBMRandomPathGenerator rpg = new GBMRandomPathGenerator(rate, N, sigma, S0, startDate, endDate, rvg);

        // Run the iteration for 100 times so y*Math.sqrt(variance/count)
        // is unlikely to coincidentally <= error
        for (int i=0;i<100;i++) sc.refresh(ec.getPayout(rpg));

        // Now continue the iteration until y*Math.sqrt(variance/count)<= error:
        while(sc.check(y,error)== false) sc.refresh(ec.getPayout(rpg));

        // discount with 252 days
        double europeprice = sc.getmean()*Math.exp(-rate*N);

        System.out.println("Price of European Call option is:");
        System.out.printf("%.4f USD with %d iterations. \n", europeprice, sc.getcount());

        long endTime   = System.currentTimeMillis();
        double totalTime = endTime - startTime;
        System.out.printf("Simulation time: %.3f seconds \n\n",totalTime/1000);

        // Q2: Asian Option

        startTime = System.currentTimeMillis();

        //change the strike price
        K = 164;

        //Get the parts ready
        sc = new StatsCollector();
        grng = new GaussianRandomNumberGenerator(N);
        rvg = new AntiTheticVectorGenerator(grng);
        AsianCallOption ac = new AsianCallOption(K);
        rpg = new GBMRandomPathGenerator(rate, N, sigma, S0,startDate, endDate, rvg);

        // Run the iteration for 100 times so y*Math.sqrt(variance/count)
        // is unlikely to coincidentally <= error
        for (int i=0;i<100;i++) sc.refresh(ac.getPayout(rpg));

        // Now continue the iteration until y*Math.sqrt(variance/count)<= error:
        while(sc.check(y,error)== false) sc.refresh(ac.getPayout(rpg));

        // discount with 252 days
        double asianprice = sc.getmean()*Math.exp(-rate*N);

        System.out.println("Price of Asian Call option is:");
        System.out.printf("%.4f USD with %d iterations. \n", asianprice, sc.getcount());

        endTime   = System.currentTimeMillis();
        totalTime = endTime - startTime;
        System.out.printf("Simulation time: %.3f seconds ",totalTime/1000);

    }
}

