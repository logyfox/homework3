package homework3;

/**
 * Created by Lipei Li on 2014/12/12.
 */

public class StatsCollector {
    double mean;
    double SS;
    double variance;
    int count;

    StatsCollector(){
        this.mean = 0;
        this.SS = 0;
        this.variance = 0;
        this.count = 0;
    }

    void refresh(double price){
        mean = mean*count/(count+1)+ price/(count+1);
        SS = SS+price*price;
        count = count+1;
        variance = SS/count - mean*mean;
    }

    double getmean(){return mean;}
    double getvariance(){return variance;}
    int getcount(){return count;}

    boolean check(double y, double error){
        return y*Math.sqrt(variance/count)<= error;
    }
}

