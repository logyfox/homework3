package homework3;

/**
 * Created by Lipei Li on 2014/12/12.
 */
public class Pair <K,V>{

    private K k;
    private V v;

    public Pair(K key, V value){
        k = key;
        v = value;
    }

    public K getKey(){ return k;}
    public V getValue(){ return v;}

}