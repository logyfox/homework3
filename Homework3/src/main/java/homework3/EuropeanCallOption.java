package homework3;

/**
 * Created by Lipei Li on 2014/12/12.
 */
import java.util.List;

import org.joda.time.DateTime;

public class EuropeanCallOption implements PayOut{
    private double K;

    public EuropeanCallOption(double K){
        this.K = K;
    }


    public double getPayout(PathGenerator path) {
        List<Pair<DateTime, Double>> prices = path.getPath();
        return Math.max(0, prices.get(prices.size()-1).getValue() - K);
    }
}
