package homework3;

/**
 * This class uses our new class UniformToGaussianTransformer
 * to produce the same result as in homework 1
 * so our rest codes need no adjustment
 *
 * Created by Lipei Li on 2014/12/12.
 */

import com.nativelibs4java.opencl.*;
import org.bridj.Pointer;

import static org.bridj.Pointer.allocateFloats;

public class GaussianRandomNumberGenerator implements RandomVectorGenerator{

    private int N;;
    private int read_position; //records our position at the 2 million Gaussians
    private UniformToGaussianTransformer transformer; //transformer that can produce Gaussians
    private Pointer<Float> gaussianPointer; // store the produced Gaussians


    public GaussianRandomNumberGenerator(int N){
        this.N = N;
        read_position = 0; // initial at first element
        transformer = new UniformToGaussianTransformer(2000000); //batch size 2 million
        gaussianPointer = transformer.getGaussianPointer(); //get the 2 million gaussians once
    }

    public double[] getVector() {
        ;
        double[] vector = new double[N]; // the output vector

        //if there are sufficient elements to get, we get 252 elements from current read_position
        if (read_position+252<2000000){
            for(int i = 0; i <N;i++){
                vector[i] = gaussianPointer.get(read_position);
                read_position++;
            }
        }else{
            // if there aren't enough elements to get, we reproduce 2 million Gaussians and read from start
            read_position = 0;
            gaussianPointer = transformer.getGaussianPointer();
            for(int i = 0; i <N;i++){
                vector[i] = gaussianPointer.get(read_position);
                read_position++;
            }
        }

        return vector;
    }
}