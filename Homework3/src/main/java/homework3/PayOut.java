package homework3;

/**
 * Created by Lipei Li on 2014/12/12.
 */
public interface PayOut {

    public double getPayout(PathGenerator path);

}