package homework3;

/**
 * Created by Lipei Li on 2014/12/12.
 */
import junit.framework.TestCase;

public class test_StatsCollector extends TestCase{

    public void testStatsCollector() {
        //print the Statistics for price series 1,2,3,4
        System.out.println("print the Stats for price series 1,2,3,4");
        StatsCollector sc = new StatsCollector();
        sc.refresh(1);
        System.out.printf("mean %f, variance %f, count %d\n",sc.getmean(),sc.getvariance(),sc.getcount());
        assertTrue(sc.getmean() == 1);
        assertTrue(sc.getvariance()==0);
        assertTrue(sc.getcount() == 1);
        sc.refresh(2);
        System.out.printf("mean %f, variance %f, count %d\n",sc.getmean(),sc.getvariance(),sc.getcount());
        assertTrue(sc.getmean() == 1.5);
        assertTrue(sc.getvariance()==0.25);
        assertTrue(sc.getcount() == 2);
        sc.refresh(3);
        System.out.printf("mean %f, variance %f, count %d\n",sc.getmean(),sc.getvariance(),sc.getcount());
        assertTrue(sc.getmean() == 2);
        assertTrue((sc.getvariance() - 0.66666666667) < 0.00001);
        assertTrue(sc.getcount()==3);
        sc.refresh(4);
        System.out.printf("mean %f, variance %f, count %d\n",sc.getmean(),sc.getvariance(),sc.getcount());
        assertTrue(sc.getmean() == 2.5);
        assertTrue((sc.getvariance() - 1.25) < 0.00001);
        assertTrue(sc.getcount()==4);

       assertTrue(!sc.check(1, 0.01));
    }

}
