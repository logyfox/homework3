package homework3;

/**
 * Created by Lipei Li on 2014/12/12.
 */
import junit.framework.TestCase;

public class test_GaussianRandomNumberGenerator extends TestCase {

    public void testGRNG() {
        GaussianRandomNumberGenerator testGaussian = new GaussianRandomNumberGenerator(10);
        double[] GaussianVector = testGaussian.getVector();

        //print the 10 generated standard normal numbers
        System.out.println("print the 10 generated standard normal numbers:");
        for (int i=0; i<GaussianVector.length;i++){
            System.out.println(GaussianVector[i]);
        }
    }

}
