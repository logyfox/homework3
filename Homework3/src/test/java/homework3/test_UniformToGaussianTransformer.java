package homework3;

/**
 * Created by Lipei Li on 2014/12/12.
 */

import junit.framework.TestCase;
import org.bridj.Pointer;

public class test_UniformToGaussianTransformer extends TestCase{
    public void test_UniformToGaussianTransformer(){
        UniformToGaussianTransformer UTG = new UniformToGaussianTransformer(1000);
        UniformToGaussianTransformer UTG1 = new UniformToGaussianTransformer(999);
        Pointer<Float> newPointer = UTG.getGaussianPointer();

        //we should have an array of length 1000 by calling toArray();
        assertTrue(newPointer.toArray().length == 1000);

        //see if the numbers get each time are truly random.
        for (int i = 995;i<1000;i++){
            System.out.println(newPointer.get(i));
        }
        newPointer = UTG.getGaussianPointer();
        for (int i = 995;i<1000;i++){
            System.out.println(newPointer.get(i));
        }
    }
}
