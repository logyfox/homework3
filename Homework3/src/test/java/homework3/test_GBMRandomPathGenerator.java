package homework3;

/**
 * Created by Lipei Li on 2014/12/12.
 */
import java.util.List;

import org.joda.time.DateTime;

import junit.framework.TestCase;

public class test_GBMRandomPathGenerator extends TestCase{

    public void testGBMRandomPathGenerator() {
        double S0 = 10;
        double sigma = 0.01;
        double rate = 0.0001;
        int N = 5;
        DateTime startDate = new DateTime(2014,12,12,11,22);
        DateTime endDate = new DateTime(2014,12,13,1,1);

        GaussianRandomNumberGenerator grng = new GaussianRandomNumberGenerator(N);
        RandomVectorGenerator rvg = new AntiTheticVectorGenerator(grng);
        GBMRandomPathGenerator rpg = new GBMRandomPathGenerator(rate, N, sigma, S0, startDate, endDate, rvg);

        List<Pair<DateTime, Double>> pg = rpg.getPath();

        //simulate 5 steps of prices
        System.out.println("simulate 5 steps of prices:");
        for (int i=0; i<pg.size();i++){
            System.out.println(pg.get(i).getValue());
        }
    }

}
