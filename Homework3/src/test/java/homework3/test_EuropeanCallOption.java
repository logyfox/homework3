package homework3;

/**
 * Created by Lipei Li on 2014/12/12.
 */
import org.joda.time.DateTime;

import junit.framework.TestCase;

public class test_EuropeanCallOption extends TestCase {

    public void testEuropeanCallOption(){
        double S0 = 152.35;
        double sigma = 0.01;
        double rate = 0.0001;
        int N = 252;
        DateTime startDate = new DateTime(2014,12,12,11,22);
        DateTime endDate = new DateTime(2014,12,13,1,1);

        GaussianRandomNumberGenerator grng = new GaussianRandomNumberGenerator(N);
        RandomVectorGenerator rvg = new AntiTheticVectorGenerator(grng);
        GBMRandomPathGenerator rpg = new GBMRandomPathGenerator(rate, N, sigma, S0, startDate, endDate, rvg);

        EuropeanCallOption ECO = new EuropeanCallOption(165);

        //print 10 simulated payouts
        System.out.println("print 10 simulated payouts");
        for (int i=0; i<10;i++)	System.out.println(ECO.getPayout(rpg));
    }

}
